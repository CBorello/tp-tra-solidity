import "@typechain/hardhat";
import "@nomiclabs/hardhat-ethers";
import "@nomiclabs/hardhat-waffle";
import "solidity-coverage";
import "hardhat-gas-reporter";
import { HardhatUserConfig } from "hardhat/config";


const config: HardhatUserConfig = {
  defaultNetwork: "hardhat",
  solidity: {
    compilers: [{ version: "0.7.3", settings: {} }],
  },
  networks: {
    hardhat: {},
    localhost: {},
    coverage: {
      url: "http://127.0.0.1:8555", // Coverage launches its own ganache-cli client
    },
    rinkeby: {
      url: "https://eth-rinkeby.alchemyapi.io/v2/REPnpYn8SQTxXsBtsxiaxbg8Hldbb4W5",
      accounts: [`0x${'9bef50e3465153e34a143f05b91c98c682b8226b3588f71fab031423a62a6948'}`],
      //Esta parte no me saleeeeee
      gasPrice: 8000000000
      }
  }
};

export default config
